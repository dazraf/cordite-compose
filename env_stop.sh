#!/bin/bash

# build environment
# usage ./env_stop.sh [<cordite image tag>] [<environment>] [<minimum-cluster>]

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
pushd ${DIR}

IMAGE_TAG=${1:-cordite/cordite:v0.3.10}
ENVIRONMENT_SLUG=${2:-dev}
MINIMUM=${3:-yes}

docker-compose -p ${ENVIRONMENT_SLUG} down

popd