#!/bin/bash

# build environment
# usage ./build_env.sh [<cordite image tag>] [<environment>] [<minimum-cluster>]

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
pushd ${DIR}

IMAGE_TAG=${1:-cordite/cordite:v0.3.10}
ENVIRONMENT_SLUG=${2:-dev}
MINIMUM=${3:-yes}

SERVICE_COUNT=$(docker-compose -p ${ENVIRONMENT_SLUG} ps -q | wc -l)
if (( ${SERVICE_COUNT} >= 4)); then 
    echo "cluster is already started"
    exit
fi

if [ $MINIMUM = "yes" ]
then
    echo "starting a minimum cordite network"
    declare -a notaries=("notary")
    declare -a nodes=("alice bob")
    declare -a ports=("8082 8083")
else
    echo "starting a full cordite network"
    declare -a notaries=("notary")
    declare -a nodes=("alice bob charlie")
    declare -a ports=("8082 8083 8084")
fi

echo -e "\xE2\x9C\x94 $(date) create environment ${ENVIRONMENT_SLUG} with image tag ${IMAGE_TAG}"
set -e

# clean up any old docker-compose
docker-compose -p ${ENVIRONMENT_SLUG} down

# start NMS (and wait for it to be ready)
# docker login network-map
docker-compose -p ${ENVIRONMENT_SLUG} up -d network-map
until docker-compose -p ${ENVIRONMENT_SLUG} logs network-map | grep -q "io.cordite.networkmap.NetworkMapApp - started"
do
    echo -e "waiting for network-map to start"
    sleep 5
done

# start databases
IMAGE_TAG=${IMAGE_TAG} docker-compose -p ${ENVIRONMENT_SLUG} up -d corda-db
until IMAGE_TAG=${IMAGE_TAG} docker-compose -p ${ENVIRONMENT_SLUG} logs corda-db | grep -q "database system is ready to accept connections"
do
echo -e "waiting for corda-db to start up and register..."
sleep 5
done

# start notaries (and wait for them to be ready)
# docker login cordite
for NOTARY in $notaries
do
    IMAGE_TAG=${IMAGE_TAG} docker-compose -p ${ENVIRONMENT_SLUG} up -d ${NOTARY}
done
for NOTARY in $notaries
do
  until IMAGE_TAG=${IMAGE_TAG} docker-compose -p ${ENVIRONMENT_SLUG} logs ${NOTARY} | grep -q "started up and registered"
  do
    echo -e "waiting for ${NOTARY} to start up and register..."
    sleep 5
  done
done

# Pause Notaries but not before downloading their NodeInfo-* and whitelist.txt
for NOTARY in $notaries
do
    NODE_ID=$(docker-compose -p ${ENVIRONMENT_SLUG} ps -q ${NOTARY})
    NODEINFO=$(docker exec ${NODE_ID} ls | grep nodeInfo-)
    docker cp ${NODE_ID}:/opt/cordite/${NODEINFO} ${NODEINFO}
    docker cp ${NODE_ID}:/opt/cordite/whitelist.txt whitelist.txt
    docker exec ${NODE_ID} rm network-parameters
    docker pause ${NODE_ID}
done

# Copy Notary NodeInfo-* and whitelist.txt to NMS
NMS_ID=$(docker-compose -p ${ENVIRONMENT_SLUG} ps -q network-map)
docker cp whitelist.txt ${NMS_ID}:/opt/cordite/db/inputs/whitelist.txt
rm whitelist.txt
echo -e "\xE2\x9C\x94 copied whitelist.txt to ${NMS_ID}"
for NODEINFO in nodeInfo-*
do
    docker cp ${NODEINFO} ${NMS_ID}:/opt/cordite/db/inputs/non-validating-notaries/${NODEINFO}
    rm ${NODEINFO}
    echo -e "\xE2\x9C\x94 copied ${NODEINFO} to ${NMS_ID}"
done

# re-start the notaries
for NOTARY in $notaries
do
    IMAGE_TAG=${IMAGE_TAG} docker-compose -p ${ENVIRONMENT_SLUG} restart ${NOTARY}
done


# start regional nodes (and wait for them to be ready)
for NODE in $nodes
do
    IMAGE_TAG=${IMAGE_TAG} docker-compose -p ${ENVIRONMENT_SLUG} up -d ${NODE}
done
for NODE in $nodes
do
  until IMAGE_TAG=${IMAGE_TAG} docker-compose -p ${ENVIRONMENT_SLUG} logs ${NODE} | grep -q "started up and registered"
  do
    echo -e "waiting for ${NODE} to start up and register..."
    sleep 5
  done
done

# test endpoints
for PORT in $ports
do
    while [[ "$(curl -sSfk -m 5 -o /dev/null -w ''%{http_code}'' https://localhost:${PORT}/api)" != "200" ]]
    do
    echo -e "waiting for ${PORT} to return 200..."
    sleep 5
    done
done

popd

echo -e "\xE2\x9C\x94 $(date) created environment ${ENVIRONMENT_SLUG} with image tag ${IMAGE_TAG}"