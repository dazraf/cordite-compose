#!/bin/bash

IMAGE_COUNT=$(docker ps -q | wc -l)
if [[ ${IMAGE_COUNT} -eq "0" ]]; then 
    echo no running containers
else 
    echo "cleaning up ${IMAGE_COUNT} running docker images"
    docker stop $(docker ps -q)
fi

IMAGE_COUNT=$(docker ps -aq | wc -l)
if [[ ${IMAGE_COUNT} -eq  "0" ]]; then 
    echo no stopped containers
else
    echo "cleaning up ${IMAGE_COUNT} stopped docker images"
    docker rm $(docker ps -aq)
fi