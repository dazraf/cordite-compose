/**
The following sets up the databases required by each corda network in the docker-compose test cluster
*/
CREATE DATABASE alice;
CREATE DATABASE bob;
CREATE DATABASE charlie;
CREATE DATABASE notary;
