<!--

      Copyright 2018, Cordite Foundation.

       Licensed under the Apache License, Version 2.0 (the "License");
       you may not use this file except in compliance with the License.
       You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing, software
       distributed under the License is distributed on an "AS IS" BASIS,
       WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
       See the License for the specific language governing permissions and
       limitations under the License.

-->
## Testing in Cordite

From this directory, you will be able to start up a local Cordite environment with a Network Map Service, all running in docker. The docker setup for this environment is specified in the `docker-compose.yml` file.
The configuration runs corda with the official Postgres image, using one database engine containing multiple databases, one per node.
To bootstrap the database the file `db-init/init-corda-db.sql` is run on the database using Postgres image's documented technique. 

## Cluster Nodes

* Notary
* Alice
* Bob
* Charlie (only for full networks - see below for how to activate the full network)

## Starting the network

1. Make sure you have an up-to-date version of Docker installed. This repo has been tested with:

* Docker Engine: `18.09.2`
* Docker Compose: `1.23.2`

2. You will need bash. For Windows users, this repo has been tested on `GitBash` installable from [here](https://gitforwindows.org/). This repo has not been tested with `Cygwin`

3. Execute 

```bash
./env_start.sh
```

You can execute this with explicit parameters:

```
./env_start.sh [<cordite image tag>] [<environment>] [<minimum-cluster>]
```

The defaults for these parameters are:

* `<cordite image tag>`: `cordite/cordite:v0.3.10`
* `<environment>`: `dev` This is the name of the docker network that these nodes will be created within.
* `<minimum-cluster>`: `yes` A minimum cluster will have `Alice`, `Bob` and a `Notary`. A full cluster will also have a node `Charlie`.

## Stopping the cluster

```bash
./env_stop.sh
```
