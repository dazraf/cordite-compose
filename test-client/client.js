const Proxy = require('braid-client').Proxy;

const alice = new Proxy({url: 'https://localhost:8082/api/'}, onOpen, onClose, onError, {strictSSL: false})

function onOpen() {
    console.log("Opened");
    alice.ledger.listAccounts().then(accounts => {
      console.log("accounts", accounts);
    }).catch(error => {
      console.error(error);s
    });
}

function onClose() {
    console.log("Closed");
}

function onError(err) {
    console.error(err);
}
